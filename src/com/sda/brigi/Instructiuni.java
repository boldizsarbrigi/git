package com.sda.brigi;

public class Instructiuni {

	public static void main(String[] args) {

		// instructiuni
		int[] agesArray = { 11, 22, 33, 44, 55, 66, 77, 88 };

		for (int i = 0; i < agesArray.length; i++) {
			// System.out.println("For: " + agesArray[i]);

			printing(agesArray[i]);
		}

		int j = 0;
		while (j < agesArray.length) {
			System.out.println("While: " + agesArray[j]);
			j++; // ultima line trebuie sa fie totdeauna incrementarea
		}

		int k = 0;
		do {
			System.out.println("Do While: " + agesArray[k]);
			k++; // ultima line trebuie sa fie totdeauna incrementarea
		} while (k < agesArray.length);

		// metode
		printing(agesArray[0]);
	}

	// metode
	public static void printing(int number) {
		System.out.println("Element: " + number);
	}

	public static String sendMessage(int number) {
		return "Element: " + number; // string + number va deveni un string si de accea merge
	}
}