package com.sda.brigi;
/*Write a program which takes 2 digits, X,Y as input and generates a 2-dimensional array. The element value in the i-th row and j-th column of the array should be i*j.
Example
Suppose the following inputs are given to the program:
3,5
Then, the output of the program should be:
[0, 0, 0, 0, 0]
[0, 1, 2, 3, 4]
[0, 2, 4, 6, 8] */

public class ArrayDimensional {

	public static void main(String[] args) {

		int iSize = 3;
		int jSize = 5;
		for (int i = 0; i < iSize; i++) {
			System.out.print(" [ ");
			for (int j = 0; j < jSize; j++) {
				if (j==4) {
				System.out.print(i * j );
			}else {
				System.out.print(i * j + ", ");
			}
			
		}
			System.out.println(" ] ");
	}
}
}
