package com.sda.brigi;

import java.util.Scanner;

public class SumaNumere {

	public static void main(String[] args) {

		// numberSum();
		// numberSumUsingFor();

		int suma = numberSumUsingFor();
		System.out.println("Sum of numbers: " + suma);

	}

	public static void numberSum() {
		int sum = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a1 number: ");
		int a1 = scanner.nextInt();
		System.out.print("Enter a2 number: ");
		int a2 = scanner.nextInt();
		System.out.print("Enter a3 number: ");
		int a3 = scanner.nextInt();
		System.out.print("Enter a4 number: ");
		int a4 = scanner.nextInt();
		System.out.print("Enter a5 number: ");
		int a5 = scanner.nextInt();

		sum = a1 + a2 + a3 + a4 + a5;
		System.out.println("Sum of numbers: " + sum);
	}

	public static int numberSumUsingFor() {

		Scanner scanner = new Scanner(System.in);

		System.out.print("How many numbers do you want to enter? ");
		int sum = 0;
		int x = scanner.nextInt();

		int[] myArray = new int[x];

		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		for (int i = 0; i < myArray.length; i++) {
			System.out.print("Enter a number: ");
			int number = scanner.nextInt();
			sum = sum + number;
			
			myArray[i] = number;
			if (myArray[i] <= min) {
				min = myArray[i];
			}
			
			myArray[i] = number;
			if (myArray[i] >= max) {
				max = myArray[i];
			}

		}
		System.out.println("min number is :" + min);
		System.out.println("max number is :" + max);
		return sum;
		// System.out.println("Sum of numbers: " + sum);

	}

}
