package com.sda.brigi;

/* Write a program which will find all numbers which are divisible by 7 but are not a multiple of 5,
 *  between 2177 and 3262(both included).
 *  The numbers obtained should be printed in a comma-separated sequence on a single line in descending order
 */

public class Division {

	public static void main(String[] args) {
		boolean isLast = false;
		int lastNumber = -1;
		for (int i = 3262; i >= 2177; i--) {
			if ((i % 7 == 0) && (i % 5 != 0)) {
				if (i == 2177) {
					System.out.print(i);
				} else {
					System.out.print(i + ", ");
				}

			}

		}

	}
}