package com.sda.brigi;

import java.util.Scanner;
import java.util.Random;

/*
 Write a Guess the Number game that has three levels of difficulty. The first level of difficulty would be a number between 1 and 10. The second difficulty
set would be between 1 and 100. The third difficulty set would be between 1 and 1000. Prompt for the difficulty level, and then start the game.The computer picks a random number in that range and prompts the player to guess that number. Each time the player guesses, the compute should give the player a hint as to whether the number is too high or too low. The computer should also keep track of the number of guesses. Once the player guesses the correct number, the computer should present the number of guesses and ask if the player would like to play again.

Let's play Guess the Number.
Pick a difficulty level (1, 2, or 3): 1
I have my number.
What's your guess?
1
Too low. Guess again: 
5
Too high. Guess again:
2
You got it in 3 guesses!
Play again? n
Goodbye!
 */
public class GuessNumber {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		int numberOfGuesses = 0;
		Random rand = new Random();

		System.out.println("Pick a difficulty level between 1, 2 and 3: ");
		String difficulty = scanner.nextLine();

		if (Integer.parseInt(difficulty) == 1 || Integer.parseInt(difficulty) == 2
				|| Integer.parseInt(difficulty) == 3) {
			System.out.println("Difficulty level picked: " + difficulty);

			if (Integer.parseInt(difficulty) == 1) {
				int numberRandom = rand.nextInt(10);
				System.out.println("I have my number: " + numberRandom);
				System.out.println("What is your guess? ");
				int guess = scanner.nextInt();
				while (numberRandom != guess) {
					guess = scanner.nextInt();
					if (guess == numberRandom) {
						System.out.println("Your number is correct ");
					} else if (guess > numberRandom) {
						System.out.println("The number is too high. Guess again. ");
					} else if (guess < numberRandom) {
						System.out.println("The number is too low. Guess again.");
					} else {
						System.out.println("Try again: ");
					}
					numberOfGuesses++;
				}
				System.out.println("number of guesses: " + numberOfGuesses);
				if (numberOfGuesses == 1) {
					System.out.println("You're a mind reader");
				} else if (numberOfGuesses >= 2 && numberOfGuesses <= 4) {
					System.out.println("Most impressive");
				} else if (numberOfGuesses >= 3 && numberOfGuesses <= 6) {
					System.out.println("You can do better than that.");
				} else {
					System.out.println("Better luck next time");

				}

			} else if (Integer.parseInt(difficulty) == 2) {
				int numberRandom = rand.nextInt(100);
				System.out.println("I have my number: " + numberRandom);
				System.out.println("What is your guess? ");
				int guess = scanner.nextInt();
				while (guess != numberRandom) {
					guess = scanner.nextInt();
					if (guess == numberRandom) {
						System.out.println("Your number is correct ");
					} else if (guess > numberRandom) {
						System.out.println("The number is too high. Guess again. ");
					} else if (guess < numberRandom) {
						System.out.println("The number is too low. Guess again.");
					} else {
						System.out.println("Try again: ");
					}
					numberOfGuesses++;
				}
				System.out.println("number of guesses: " + numberOfGuesses);
				if (numberOfGuesses == 1) {
					System.out.println("You're a mind reader");
				} else if (numberOfGuesses >= 2 && numberOfGuesses <= 4) {
					System.out.println("Most impressive");
				} else if (numberOfGuesses >= 3 && numberOfGuesses <= 6) {
					System.out.println("You can do better than that.");
				} else {
					System.out.println("Better luck next time");

				}
			} else if (Integer.parseInt(difficulty) == 3) {
				int numberRandom = rand.nextInt(1000);
				System.out.println("I have my number: " + numberRandom);
				System.out.println("What is your guess? ");
				int guess = scanner.nextInt();
				while (guess != numberRandom) {
					guess = scanner.nextInt();
					if (guess == numberRandom) {
						System.out.println("Your number is correct ");
					} else if (guess > numberRandom) {
						System.out.println("The number is too high. Guess again. ");
					} else if (guess < numberRandom) {
						System.out.println("The number is too low. Guess again.");
					} else {
						System.out.println("Try again: ");
					}
					numberOfGuesses++;
				}
				System.out.println("number of guesses: " + numberOfGuesses);
				if (numberOfGuesses == 1) {
					System.out.println("You're a mind reader");
				} else if (numberOfGuesses >= 2 && numberOfGuesses <= 4) {
					System.out.println("Most impressive");
				} else if (numberOfGuesses >= 3 && numberOfGuesses <= 6) {
					System.out.println("You can do better than that.");
				} else {
					System.out.println("Better luck next time");

				}

			} else {
				System.out.println("difficulty invalid: " + difficulty);
			}
		}
	}
}
