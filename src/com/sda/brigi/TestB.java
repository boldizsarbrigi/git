package com.sda.brigi;

import java.util.Random;
import java.util.Scanner;

public class TestB {
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		int wrongGuesses = 0;
		int numberOfGuesses = 0;
		Random rand = new Random();

		System.out.println("Pick a difficulty level between 1, 2 and 3: ");
		String difficulty = scanner.nextLine();

		if (Integer.parseInt(difficulty) == 1 || Integer.parseInt(difficulty) == 2
				|| Integer.parseInt(difficulty) == 3) {
			System.out.println("Difficulty level picked: " + difficulty);

			if (Integer.parseInt(difficulty) == 1) {
				int numberRandom = rand.nextInt(10);
				System.out.println("I have my number: " + numberRandom);
				System.out.println("What is your guess? ");
				int guess = scanner.nextInt();
				
				int[] previousGuesses = new int[guess];
				for (int i=0; i!=numberRandom; i++) {
					previousGuesses[i] = scanner.nextInt();
					
					while (numberRandom != guess) {
						guess = scanner.nextInt();
							
							
						if (guess == numberRandom) {
							System.out.println("Your number is correct ");
						} else if (guess > numberRandom && previousGuesses[i] != guess) {
							System.out.println("The number is too high. Guess again. ");

						} else if (guess < numberRandom && previousGuesses[i] != guess) {
							System.out.println("The number is too low. Guess again.");

						} else if (previousGuesses[i] == guess) {
							System.out.println("you've already guessed this number, try another");
							wrongGuesses++;
						} else {
							System.out.println("Try again: ");
						}
						
					}numberOfGuesses++;
				}
			}
				System.out.println("Same guessses counted as wrong numbers: " + wrongGuesses);
				System.out.println("number of guesses: " + numberOfGuesses);
				if (numberOfGuesses == 1) {
					System.out.println("You're a mind reader");
				} else if (numberOfGuesses >= 2 && numberOfGuesses <= 4) {
					System.out.println("Most impressive");
				} else if (numberOfGuesses >= 3 && numberOfGuesses <= 6) {
					System.out.println("You can do better than that.");
				} else {
					System.out.println("Better luck next time");
				}
			
		} else if (Integer.parseInt(difficulty) == 2) {
			int numberRandom = rand.nextInt(100);
			System.out.println("I have my number: " + numberRandom);
			System.out.println("What is your guess? ");
			int guess = scanner.nextInt();
			while (guess != numberRandom) {
				guess = scanner.nextInt();
				if (guess == numberRandom) {
					System.out.println("Your number is correct ");
				} else if (guess > numberRandom) {
					System.out.println("The number is too high. Guess again. ");
				} else if (guess < numberRandom) {
					System.out.println("The number is too low. Guess again.");
				} else {
					System.out.println("Try again: ");
				}
				numberOfGuesses++;
			}
			System.out.println("number of guesses: " + numberOfGuesses);
			if (numberOfGuesses == 1) {
				System.out.println("You're a mind reader");
			} else if (numberOfGuesses >= 2 && numberOfGuesses <= 4) {
				System.out.println("Most impressive");
			} else if (numberOfGuesses >= 3 && numberOfGuesses <= 6) {
				System.out.println("You can do better than that.");
			} else {
				System.out.println("Better luck next time");

			}
		} else if (Integer.parseInt(difficulty) == 3) {
			int numberRandom = rand.nextInt(1000);
			System.out.println("I have my number: " + numberRandom);
			System.out.println("What is your guess? ");
			int guess = scanner.nextInt();
			while (guess != numberRandom) {
				guess = scanner.nextInt();
				if (guess == numberRandom) {
					System.out.println("Your number is correct ");
				} else if (guess > numberRandom) {
					System.out.println("The number is too high. Guess again. ");
				} else if (guess < numberRandom) {
					System.out.println("The number is too low. Guess again.");
				} else {
					System.out.println("Try again: ");
				}
				numberOfGuesses++;
			}
			System.out.println("number of guesses: " + numberOfGuesses);
			if (numberOfGuesses == 1) {
				System.out.println("You're a mind reader");
			} else if (numberOfGuesses >= 2 && numberOfGuesses <= 4) {
				System.out.println("Most impressive");
			} else if (numberOfGuesses >= 3 && numberOfGuesses <= 6) {
				System.out.println("You can do better than that.");
			} else {
				System.out.println("Better luck next time");

			}

		} else {
			System.out.println("difficulty invalid: " + difficulty);
		}
	}
}
