package com.sda.brigi;

/*Write a program that accepts a sentence and calculate the number of letters, digits and other characters.
Suppose the following input is supplied to the program:
hello world! 123
Then, the output should be:
LETTERS 10
DIGITS 3
OTHER 3
 */
import java.util.Scanner;

public class StringRead {

	private static Scanner scanner = new Scanner(System.in); //defineste clasa pentru citirea din consola System.in

	public static void main(String[] args) {
		System.out.println("Enter sentence: ");
		
		String sentence = scanner.nextLine(); // citeste linia cand dam enter
		
		System.out.println("input =  " + sentence); // afiseaza ce am scris in consola

		// String sentence = "hello world! 123";
		int countLetter = 0;
		int countDigits = 0;
		int countOther = 0;
		// System.out.println(sentence);
		for (int i = 0; i < sentence.length(); i++) {
			if (Character.isLetter(sentence.charAt(i))) {
				// if ()
				countLetter++;
			} else if (Character.isDigit(sentence.charAt(i))) {
				countDigits++;
			} else {
				countOther++;
			}
		}
		System.out.println("Letters: " + countLetter);
		System.out.println("Digits: " + countDigits);
		System.out.println("Other: " + countOther);
	}
}
